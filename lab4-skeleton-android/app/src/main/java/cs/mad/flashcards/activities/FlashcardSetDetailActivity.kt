package cs.mad.flashcards.activities

import android.content.DialogInterface
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.Flashcard


class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.flashcardList.adapter = FlashcardAdapter(Flashcard.getHardcodedFlashcards())

        binding.addFlashcardButton.setOnClickListener {
            (binding.flashcardList.adapter as FlashcardAdapter).addItem(Flashcard("test", "test"))
            binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)

            binding.termButton.setOnClickListener {
                showStandardDialog()
            }
        }
    }

    private fun showStandardDialog() {
        AlertDialog.Builder(this)
            .setTitle("Term")
            .setMessage("Defintion")
            .setPositiveButton("Delete") { dialogInterface: DialogInterface, i: Int ->
                // do something on click
            }
            .setPositiveButton("Edit") { dialogInterface: DialogInterface, i: Int ->
                // do something on click
            }
            .create()
            .show()
    }
    private fun showCustomDialog() {
        val titleView = layoutInflater.inflate(R.layout.custom_term, null)
        val bodyView = layoutInflater.inflate(R.layout.custom_definition, null)
        val titleEditText = titleView.findViewById<EditText>(R.id.custom_term)
        val bodyEditText = bodyView.findViewById<EditText>(R.id.custom_defintiion)
        titleEditText.setText("Term")
        bodyEditText.setText("Definition")

        AlertDialog.Builder(this)
            .setCustomTitle(titleView)
            .setView(bodyView)
            .setPositiveButton("Delete") { dialogInterface: DialogInterface, i: Int ->
                // do something on click
                Snackbar.make(binding.root, titleEditText.text.toString(), Snackbar.LENGTH_LONG)
                    .show()
            }
            .create()
            .show()
    }
}
